import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {USER} from './user.model';
import {REVIEW} from './review.model';
@Injectable({ providedIn: 'root'})

export class UserService {
  private userinfo: USER[] = [];

  constructor(private http: HttpClient) {}

  addUser(firstName: string,
          lastName: string,
          emailId: string,
          password: string) {
      const user: USER = {id: null, firstName, lastName,
        emailId,
        password};
      this.http
        .post<{message: string}>('http://localhost:3000/api/new-usersc', user)
        .subscribe(responseData => {
          console.log(responseData.message);
          this.userinfo.push(user);
        });
  }

contactus(name: string, emailid: string , message: string) {

  const cont: REVIEW = {id: null, name, emailid, message};
  this.http
  .post<{message: string}>('http://localhost:3000/api/review', cont)
  .subscribe(responseData => {
    console.log(responseData.message);

  });


}


 }

