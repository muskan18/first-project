import { Component, OnInit, Inject } from '@angular/core';
import { UserService } from '../user.service';
import { Route } from '@angular/compiler/src/core';
import { Router } from '@angular/router';
import { FormGroup } from '@angular/forms';

@Component({

  selector: 'app-sign',
  templateUrl: './sign.component.html',
  styleUrls: ['./sign.component.css']
})
export class SignComponent implements OnInit {

  constructor( private myservice: UserService, private router: Router) { }

  fname;
  lname;
  email;
  password;


  cancel() {
    this.router.navigateByUrl('');
  }
  register() {
// tslint:disable-next-line: triple-equals

if (this.fname == null || this.lname == null || this.email == null || this.password == null) {
  alert('Fields can not be empty !! ');
 // window.location.reload();
} else {
 // tslint:disable-next-line: triple-equals
 if (this.fname != 'admin') {

  this.myservice.addUser(this.fname, this.lname, this.email, this.password);
  alert('Registeration Successfull ! ');
  this.router.navigateByUrl('second');
 } else {
   alert('Firstname can not be admin !!');
   window.location.reload();
   // this.signupform.reset();
     }

  }
  }

  ngOnInit() {
  }

}
