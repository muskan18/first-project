import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MainpComponent } from './mainp/mainp.component';
import { LogiComponent } from './logi/logi.component';
import { SignComponent } from './sign/sign.component';
import { NbComponent } from './nb/nb.component';
import { FootComponent } from './foot/foot.component';
import { MaterialModule } from './material.module';
import {MatSidenavModule} from '@angular/material/sidenav';
import { RouterModule, Routes } from '@angular/router';
import { CourseComponent } from './course/course.component';
import { ContactComponent } from './contact/contact.component';
import { SecondComponent } from './second/second.component';
import { AdminComponent } from './admin/admin.component';
import { UserService } from './user.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatDialogModule } from '@angular/material';
import { HttpClientModule } from '@angular/common/http';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    MainpComponent,
    LogiComponent,
    SignComponent,
    NbComponent,
    FootComponent,
    CourseComponent,
    ContactComponent,
    SecondComponent,
    AdminComponent,
    HomeComponent
    ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    MatDialogModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})

export class AppModule {

  constructor() {
    }

}
