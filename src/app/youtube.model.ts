export interface YOUTUBE {

  id: string;
  name: string;
  links: string;
}
