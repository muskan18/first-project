import { Component, OnInit } from '@angular/core';
import {AdminService } from '../admin.service';
import {Router} from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {REVIEW} from '../review.model';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  subjectsel;
  contentsel ;
  title ;
  desc;
  link;
  data;
  rev: REVIEW[] = [];

titlehandler(event: any) {
this.title = event.target.value;
}
deschandler(event: any) {
this.desc = event.target.value;
}
linkhandler(event: any) {
this.link = event.target.value;
}


  constructor(private myservice: AdminService , private router: Router , private http: HttpClient) {

    this.http
    .get<{message: string, rev: any}>('http://localhost:3000/api/review')
    .subscribe(responseData => {

      console.log(responseData.rev);
      this.data = responseData.rev;

    });
  }

  oncancel() {
    this.router.navigateByUrl('');
  }
  onAdd() {
    if ((this.subjectsel != null) && (this.contentsel != null) && (this.title != null) && (this.desc != null) && ( this.link != null)) {

      // tslint:disable-next-line: triple-equals
      if (this.contentsel == 'course') {

        this.myservice.addCourse(this.subjectsel, this.title, this.desc, this.link);
      }

      // tslint:disable-next-line: triple-equals
      if (this.contentsel == 'blogs') {

        this.myservice.addBlogs(this.subjectsel, this.title, this.desc, this.link);
}

      alert('Data added successfully !');

      window.location.reload();
      // this.router.navigateByUrl('adpage');

    } else {
      alert('Please enter all the details !');
    }
  }
  ngOnInit() {
  }

}
