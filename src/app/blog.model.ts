export interface BLOG {

  id: string;
  name: string;
  title: string;
  description: string;
  links: string;

  }
