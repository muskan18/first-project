import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

  fname;
  email;
  message;

  constructor(private myservice: UserService) { }

  contact(){

if((this.fname==null)||(this.email==null)||(this.message == null)){

  alert('Please enter all the details ! ');

  }

  else{

this.myservice.contactus(this.fname,this.email,this.message);
alert('Message submitted Successfully ! ');
window.location.reload();
  }
}
  ngOnInit() {
  }

}
