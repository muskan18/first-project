import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import {YOUTUBE} from '../youtube.model';
import {HttpClient} from '@angular/common/http';
import {Router} from '@angular/router';
import {COURSES} from '../courses.model';
import {BLOG} from '../blog.model';


@Component({
  selector: 'app-second',
  templateUrl: './second.component.html',
  styleUrls: ['./second.component.css']
})
export class SecondComponent implements OnInit {

  contentc: string[] = ['Courses', 'Blogs', 'Youtube'];
  subject: string[] = ['Angular', 'Java', 'Node', 'JavaScript'];
  filterch: string;
  subjectchoosen: string;
  thumbnail;
  name;
  vall;
  random;
  linkk = [];
bchoosen = false;
cchoosen = false;
ychoosenv = false;
  public crs: COURSES[] = [];
  public  blg: BLOG[] = [];
  public yb: YOUTUBE[] = [];
data;
  safeSrc: SafeResourceUrl;

  constructor(public sanitizer: DomSanitizer, private router: Router , private http: HttpClient) {
    // alert('Choose a subject first and then choose what you want i.e courses or blogs or youtube channels' + '\n' + 'Thankyou !');

  //  this.ychoosenv = true;
     this.filterch = 'Courses';
      this.chosen('java');

    }


subjectHandler(event: any) {

  this.subjectchoosen = event.target.value;
  this.cchoosen = false;
  this.ychoosenv = false;
  this.bchoosen = false;

  // alert('You have choosen : ' + this.subjectchoosen);
}
  radioChangeHandler(event: any) {

     this.filterch = event.target.value;
     alert('You have choosen to view :  ' +  this.filterch + ' of ' + this.subjectchoosen);

     this.vall = this.subjectchoosen.toLowerCase();
     this.chosen(this.vall);

     this.cchoosen = false;
     this.ychoosenv = false;
     this.bchoosen = false;
    }

chosen(name) {

  switch (this.filterch) {
      case 'Courses': {

          this.http
        .get<{message: string, crs: any}>('http://localhost:3000/api/course/' + name)
        .subscribe(responseData => {

          console.log(responseData.crs);
          this.data = responseData.crs;
          this.cchoosen = true;

        });

          break;
        }

        case 'Blogs': {

          this.http
          .get<{message: string, blg: any}>('http://localhost:3000/api/blogg/' + name)
          .subscribe(responseData => {

            console.log(responseData.blg);
            this.data = responseData.blg;
            this.bchoosen = true;
          });

          break;
        }
        case 'Youtube': {

          this.http
          .get<{message: string, yb: any}>('http://localhost:3000/api/youtube/' + name)
          .subscribe(responseData => {

            console.log(responseData.yb);
            this.data = responseData.yb;
            console.log(this.data);
           // console.log(this.data.links);

            this.ychoosenv = true;
          });

          break;
        }
    }
  }


blochoosen() {

  // tslint:disable-next-line: triple-equals
  if (this.vall == 'Blogs') {
   return true;
  }
}

coursechoosen() {

  // tslint:disable-next-line: triple-equals
  if (this.vall == 'Courses') {
  return true;
  }
}

ybchoosen() {

  // tslint:disable-next-line: triple-equals
  if (this.vall == 'Youtube') {

    // tslint:disable-next-line: prefer-for-of

//  this.safeSrc =  this.sanitizer.bypassSecurityTrustResourceUrl(this.data.links[0]);

    return true;
}
}

linkurl() {
   return this.sanitizer.bypassSecurityTrustResourceUrl(this.data.links[0]);
}

ngOnInit() {

  }

}
