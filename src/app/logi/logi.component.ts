import { Component, OnInit , Inject} from '@angular/core';
import {Router} from '@angular/router';
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-logi',
  templateUrl: './logi.component.html',
  styleUrls: ['./logi.component.css']
})
export class LogiComponent implements OnInit {

  uname;
  password;

  cancel() {
    this.router.navigateByUrl('');
  }
  login() {
    // tslint:disable-next-line: triple-equals

    if (this.uname != null) {
    if (this.uname == 'admin' && this.password == 'admin') {

      this.router.navigateByUrl('adpage');

    } else if (this.uname == 'admin' && this.password != 'admin') {
      alert('Wrong Password ! ');
    } else {

     const post = {emailId : this.uname, password: this.password};

     this.http
        .post<{message: number}>('http://localhost:3000/api/usersc', post)
        .subscribe(responseData => {
          // tslint:disable-next-line: triple-equals
          if (responseData.message == 1) {
            alert('Login successfull ! ');
            console.log('Successfull login');

            this.router.navigateByUrl('second');
          }
          // tslint:disable-next-line: triple-equals
          if (responseData.message == 0) {
            alert('Invalid Credentials ! ');
            window.location.reload();
            console.log('User not found');
          }

        });


    }
  } else {
    alert('Username and Password can\'t be empty !');
  //  window.location.reload();
  }
  }


  constructor(private router: Router , private http: HttpClient) { }

  ngOnInit() {
  }

}
