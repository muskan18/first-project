import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SignComponent } from './sign/sign.component';
import { LogiComponent } from './logi/logi.component';
import { ContactComponent } from './contact/contact.component';
import { SecondComponent } from './second/second.component';
import { AdminComponent } from './admin/admin.component';
import { HomeComponent } from './home/home.component';


const routes: Routes = [{ path: '', component: HomeComponent, pathMatch: 'full' },

{ path: 'register', component: SignComponent },
{ path: 'login', component: LogiComponent },
{path: 'contact', component: ContactComponent},
{path: 'second', component: SecondComponent},
{path: 'adpage', component: AdminComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

