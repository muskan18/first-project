export interface REVIEW
{

  id: string;
  name: string;
  emailid: string;
  message: string;
}
