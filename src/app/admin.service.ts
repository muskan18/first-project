import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {COURSES} from './courses.model';
import {BLOG} from './blog.model';

@Injectable({
  providedIn: 'root'
})
export class AdminService {


  constructor(private http: HttpClient) { }

  addCourse(name: string,
            title: string,
            desc: string,
            link: string) {
    const cc: COURSES = {id: null , name, title, desc, link};
    this.http
    .post<{message: string}>('http://localhost:3000/api/course', cc)
    .subscribe(responseData => {
      console.log(responseData.message);

    });
  }

  addBlogs(name: string,
           title: string,
           description: string,
           links: string) {

      const bb: BLOG = {id: null, name, title, description, links};
      this.http
      .post<{message: string}>('http://localhost:3000/api/blogg', bb)
      .subscribe(responseData => {
        console.log(responseData.message);

      });



  }
}
